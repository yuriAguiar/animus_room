// Fill out your copyright notice in the Description page of Project Settings.


#include "XBotAnimInstance.h"
#include "AnimSandboxCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/PawnMovementComponent.h"

void UXBotAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
	OwningPawn = TryGetPawnOwner();
}

void UXBotAnimInstance::NativeUpdateAnimation(float DeltaTimeX)
{
	Super::NativeUpdateAnimation(DeltaTimeX);

	if (OwningPawn == nullptr) { return; }

	PawnSpeed = OwningPawn->GetVelocity().Size();
	WalkingRateScale = GetNormalizedSpeed(PawnSpeed);

	IsAirborne = OwningPawn->GetMovementComponent()->IsFalling(); // IsAirborne never changes from false for some reason
	AAnimSandboxCharacter* Character = Cast<AAnimSandboxCharacter>(OwningPawn);

	if (Character == nullptr) { return; }

	// Apply the animation rate scale only if the character is walking/running
	if (PawnSpeed > 0.1 && !IsAirborne)
	{
		Character->GetMesh()->GlobalAnimRateScale = WalkingRateScale;
	}
	else
	{
		Character->GetMesh()->GlobalAnimRateScale = 1.f;
	}

	// DebugPrint(DeltaTimeX);
}

// void UXBotAnimInstance::DebugPrint(float DeltaTimeX)
// {
// 	if (IsAirborne)
// 	{
// 		GEngine->AddOnScreenDebugMessage(INDEX_NONE, DeltaTimeX, FColor::Green, "Jump", true);
// 	}
// }

// Normalize the controller stick input to match the character speed with the animation rate
float UXBotAnimInstance::GetNormalizedSpeed(float Speed)
{
	float NormalizedValue = UKismetMathLibrary::NormalizeToRange(Speed, 170, 300);
	NormalizedValue += NormalizeToRangeOffset; // For some reason the default NormalizedValue is -1.307692

	if (NormalizedValue > 1) { NormalizedValue = 1; }

	return NormalizedValue;
}
