// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "AnimSandboxGameMode.h"
#include "AnimSandboxCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAnimSandboxGameMode::AAnimSandboxGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/BP_Cassandra"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
