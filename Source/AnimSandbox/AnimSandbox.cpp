// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "AnimSandbox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, AnimSandbox, "AnimSandbox" );
 