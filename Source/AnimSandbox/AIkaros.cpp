// Fill out your copyright notice in the Description page of Project Settings.


#include "AIkaros.h"

#include "DrawDebugHelpers.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "GameFramework/Character.h"

// Sets default values
AAIkaros::AAIkaros()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// EagleMesh Setup
	EagleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("EagleMesh"));
	EagleMesh->SetupAttachment(RootComponent);

	// EagleMesh Physics 
	EagleMesh->SetSimulatePhysics(true);
	EagleMesh->SetLinearDamping(.5f);
	EagleMesh->SetAngularDamping(2.0f);
	EagleMesh->SetEnableGravity(false);

	// EagleMesh Collision
	EagleMesh->SetGenerateOverlapEvents(false);
	EagleMesh->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;
	EagleMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	EagleMesh->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	EagleMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Overlap);
	EagleMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Overlap);

	// EagleMesh Rendering
	EagleMesh->SetVisibility(false);
	EagleMesh->SetHiddenInGame(true);

	// CameraBoom Setup
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(EagleMesh);

	// CameraBoom Camera
	CameraBoom->TargetArmLength = 300.0f;

	// CameraBoom Camera Settings
	CameraBoom->bUsePawnControlRotation = true;

	// CameraBoom Lag
	CameraBoom->bEnableCameraRotationLag = true;
	CameraBoom->CameraRotationLagSpeed = 5.0f;

	// FollowCamera Setup
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom);

	// EagleSkeleton Setup
	EagleSkeleton = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("EagleSkeleton"));
	EagleSkeleton->SetupAttachment(EagleMesh);

	// EagleSkeleton Physics
	EagleSkeleton->SetEnableGravity(false);

	// ArrowComponent Setup
	ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowComponent"));
	ArrowComponent->SetupAttachment(EagleMesh);

	// ArrowComponent Rendering
	ArrowComponent->SetVisibility(true);
	ArrowComponent->SetHiddenInGame(false);

	// Setup SpeedBoost Timeline
	SpeedBoostEffectTimeline = CreateDefaultSubobject<UTimelineComponent>(FName("SpeedBoostEffectTimeline"));
	SpeedBoostTimelineUpdateEvent.BindUFunction(this, "SpeedBoostTimelineUpdateFunc"); // Sets the ufunction for the timeline to use in it's update

	// Setup ViewZoom Timeline
	ViewZoomEffectTimeline = CreateDefaultSubobject<UTimelineComponent>(FName("ViewZoomEffectTimeline"));
	ViewZoomTimelineUpdateEvent.BindUFunction(this, "ViewZoomTimelineUpdateFunc"); // Sets the ufunction for the timeline to use in it's update
}

// Called when the game starts or when spawned
void AAIkaros::BeginPlay()
{
	Super::BeginPlay();
	SpeedBoostEffectTimeline->AddInterpFloat(SpeedBoostTimelineCurve, SpeedBoostTimelineUpdateEvent, FName("SpeedBoostEffect")); // Sets the curve for the timeline to use in it's update
	ViewZoomEffectTimeline->AddInterpFloat(ViewZoomTimelineCurve, ViewZoomTimelineUpdateEvent, FName("ViewZoomEffect")); // Sets the curve for the timeline to use in it's update
}

// Called every frame
void AAIkaros::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsPossessed)
	{
		// Constantly adjust Ikaros's Pitch and Roll to stabilize flight
		const FRotator FlightTargetRotator(DesiredPitch * PITCH_ROTATION_SPEED, 0.f, DesiredRoll * ROLL_ROTATION_SPEED);
		FRotator FlightResultRotator = FMath::RInterpTo(
			EagleMesh->GetRelativeRotation(),
			FlightTargetRotator,
			DeltaTime,
			StabilizerSpeed);

		// Only increment it's Yaw so that Ikaros can turn
		FlightResultRotator.Yaw = EagleMesh->GetRelativeRotation().Yaw + (DesiredYaw * YawRotationSpeed);
		EagleMesh->SetRelativeRotation(FlightResultRotator);

		if (bIsStationary)
		{
			// Increase Ikaros's linear damping so that it stops quickly
			EagleMesh->SetLinearDamping(StationaryLinearDamping);

			// Rotate Ikaros's pitch 45 degrees up
			const FRotator StationaryTargetRotator(PITCH_STATIONARY_ANGLE, 0.f, 0.f);
			FRotator StationaryResultRotator = FMath::RInterpTo(
				EagleMesh->GetRelativeRotation(),
				StationaryTargetRotator,
				DeltaTime,
				StabilizerSpeed);

			// Keep Ikaros's Roll and yaw
			StationaryResultRotator.Roll = EagleMesh->GetRelativeRotation().Roll;
			StationaryResultRotator.Yaw = EagleMesh->GetRelativeRotation().Yaw;
			EagleMesh->SetRelativeRotation(StationaryResultRotator);
		}
		else
		{
			// Constantly apply thrust to Ikaros's body
			EagleMesh->SetLinearDamping(MovingLinearDamping);
			EagleMesh->AddForce(EagleMesh->GetForwardVector() * Thrust);

			if (bBoostSpeed)
			{
				// Increase Icaros's thrust by ThrustMultiplier times
				EagleMesh->AddForce(EagleMesh->GetForwardVector() * (Thrust * ThrustMultiplier));
			}
		}
	}

	if (bIsPossessed && bEnableDebugMessages)
	{
		DebugPrint();
	}
}

// Called to bind functionality to input
void AAIkaros::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("MoveForward", this, &AAIkaros::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AAIkaros::MoveRight);

	PlayerInputComponent->BindAction("BoostFlightSpeed", IE_Pressed, this, &AAIkaros::EnableFlightBoostCameraEffect);
	PlayerInputComponent->BindAction("BoostFlightSpeed", IE_Released, this, &AAIkaros::DisableFlightBoostCameraEffect);
	PlayerInputComponent->BindAction("FlightZoomView", IE_Pressed, this, &AAIkaros::EnableViewZoomCameraEffect);
	PlayerInputComponent->BindAction("FlightZoomView", IE_Released, this, &AAIkaros::DisableViewZoomCameraEffect);
	PlayerInputComponent->BindAction("StationaryMode", IE_Pressed, this, &AAIkaros::EnableStationaryMode);
	PlayerInputComponent->BindAction("StationaryMode", IE_Released, this, &AAIkaros::DisableStationaryMode);
	PlayerInputComponent->BindAction("TagEnemy", IE_Pressed, this, &AAIkaros::TagEnemy);
	PlayerInputComponent->BindAction("TagEnemy", IE_Released, this, &AAIkaros::ReleaseTagEnemyButton);
}

void AAIkaros::MoveForward(float Value)
{
	DesiredPitch = Value;
}

void AAIkaros::MoveRight(float Value)
{
	DesiredRoll = Value;
	DesiredYaw = Value;
}

void AAIkaros::EnableFlightBoostCameraEffect()
{
	bBoostSpeed = true;

	if (!SpeedBoostTimelineSwitch) { return; }

	CameraShakeRef = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->PlayCameraShake(CameraShakeClass);

	if (!bIsZooming)
	{
		
		SpeedBoostEffectTimeline->Stop();
		SpeedBoostEffectTimeline->PlayFromStart();
	}

	SpeedBoostTimelineSwitch = false;
}

void AAIkaros::DisableFlightBoostCameraEffect()
{
	bBoostSpeed = false;

	if (CameraShakeRef != nullptr)
	{
		UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->StopCameraShake(CameraShakeRef);
	}

	if (!bIsZooming)
	{
		SpeedBoostEffectTimeline->Stop();
		SpeedBoostEffectTimeline->PlayFromStart();
	}
	
	SpeedBoostTimelineSwitch = true;
}

void AAIkaros::EnableViewZoomCameraEffect()
{
	bIsZooming = true;
	ViewZoomEffectTimeline->Stop();
	ViewZoomEffectTimeline->PlayFromStart();
}

void AAIkaros::DisableViewZoomCameraEffect()
{
	bIsZooming = false;
	ViewZoomEffectTimeline->Stop();
	ViewZoomEffectTimeline->PlayFromStart();
}

void AAIkaros::EnableStationaryMode()
{
	bIsStationary = true;
}

void AAIkaros::DisableStationaryMode()
{
	bIsStationary = false;
}

void AAIkaros::TagEnemy()
{
	bIsTagging = true;

	const FVector SphereTraceVectorEnd = FollowCamera->GetForwardVector() * TagSphereTraceLengthMultiplier + FollowCamera->GetComponentLocation(); // The end point of the vector pointing to where the FollowCamera is looking
	const TArray<AActor*> ActorsToIgnore; // Not used
	FHitResult HitResult;
	// Casts a sphere trace to check if the hit actor is an enemy. The TraceTypeQuery1 refers to the custom created trace channel TagTrace
	UKismetSystemLibrary::SphereTraceSingle(
		GetWorld(),
		FollowCamera->GetComponentLocation(),
		SphereTraceVectorEnd,
		SPHERE_TRACE_RADIUS,
		ETraceTypeQuery::TraceTypeQuery1,
		false,
		ActorsToIgnore,
		EDrawDebugTrace::None,
		HitResult,
		true);

	TArray<USceneComponent*> Components; // Used USceneComponent array because UWidgetComponent has a compiler link problem with the GetStaticClass method
	
	if (!HitResult.Actor.IsValid()) { return; }
	
	HitResult.Actor->GetComponents(Components);

	for (auto Component : Components)
	{
		if (Component->ComponentHasTag(FName("TagWidget")) && HitResult.Actor != nullptr && HitResult.Actor->ActorHasTag(EnemyTag))
		{
			Component->SetVisibility(true);
			return;
		}
	}
}

void AAIkaros::ReleaseTagEnemyButton()
{
	bIsTagging = false;
}

// UFunction called in the SpeedBoost effect timeline's update
void AAIkaros::SpeedBoostTimelineUpdateFunc(float DeltaSeconds) const
{
	float Value;
	
	if (bBoostSpeed)
	{
		Value = FMath::Lerp(FollowCamera->FieldOfView, 85.f, SpeedBoostEffectTimeline->GetPlaybackPosition());
	}
	else
	{
		Value = FMath::Lerp(FollowCamera->FieldOfView, DEFAULT_FIELD_OF_VIEW, SpeedBoostEffectTimeline->GetPlaybackPosition());
	}

	FollowCamera->SetFieldOfView(Value);
}

// UFunction called in the ViewZoom effect timeline's update
void AAIkaros::ViewZoomTimelineUpdateFunc(float DeltaSeconds) const
{
	float Value;
	
	if (bIsZooming)
	{
		Value = FMath::Lerp(FollowCamera->FieldOfView, 70.f, ViewZoomEffectTimeline->GetPlaybackPosition());
	}
	else
	{
		Value = FMath::Lerp(FollowCamera->FieldOfView, DEFAULT_FIELD_OF_VIEW, ViewZoomEffectTimeline->GetPlaybackPosition());
	}

	FollowCamera->SetFieldOfView(Value);
}

void AAIkaros::DebugPrint() const
{
	DrawDebugString(GetWorld(),
        FVector(0.f, 0.f, 0.f),
        FString("Ikaros"),
        EagleMesh->GetOwner(),
        FColor::Green,
        0.f);

	if (bIsStationary) { SetTextAndColorToDebugAndPrint(FString("Ctrl Key - Stationary Mode: Pressed"), FColor::Yellow); }
	else { SetTextAndColorToDebugAndPrint(FString("Ctrl Key - Stationary Mode: Released"), FColor::Green); }

	if (bBoostSpeed) { SetTextAndColorToDebugAndPrint(FString("Shift Key - Boost Speed: Pressed"), FColor::Yellow); }
	else { SetTextAndColorToDebugAndPrint(FString("Shift Key - Boost Speed: Released"), FColor::Green); }

	if (bIsZooming) { SetTextAndColorToDebugAndPrint(FString("Right Mouse Key - Zoom View: Pressed"), FColor::Yellow); }
	else { SetTextAndColorToDebugAndPrint(FString("Right Mouse Key - Zoom View: Released"), FColor::Green); }

	if (bIsTagging) { SetTextAndColorToDebugAndPrint(FString("Left Mouse Key - Tag Enemy: Pressed"), FColor::Yellow); }
	else { SetTextAndColorToDebugAndPrint(FString("Left Mouse Key - Tag Enemy: Released"), FColor::Green); }

	const FString IkarosDirectionAndRotationString = FString("Forward vector: ").
	Append(LINE_TERMINATOR).
	Append(EagleMesh->GetForwardVector().ToString())
	.Append(LINE_TERMINATOR)
	.Append(LINE_TERMINATOR)
	.Append("Relative rotation:")
	.Append(LINE_TERMINATOR)
	.Append("Roll: ")
	.Append(FString::SanitizeFloat(EagleMesh->GetRelativeRotation().Roll))
	.Append(" Pitch: ")
	.Append(FString::SanitizeFloat(EagleMesh->GetRelativeRotation().Pitch))
	.Append(" Yaw: ")
	.Append(FString::SanitizeFloat(EagleMesh->GetRelativeRotation().Yaw))
	.Append(LINE_TERMINATOR)
	.Append(LINE_TERMINATOR)
	.Append("Linear velocity:")
	.Append(LINE_TERMINATOR)
	.Append(EagleMesh->GetPhysicsLinearVelocity().ToString());
	SetTextAndColorToDebugAndPrint(IkarosDirectionAndRotationString, FColor::Green);
}

void AAIkaros::SetTextAndColorToDebugAndPrint(const FString DebugString, const FColor DebugColor) const
{
	UKismetSystemLibrary::PrintString(
		GetWorld(),
        DebugString,
        true,
        false,
        DebugColor,
        0.f);
}
