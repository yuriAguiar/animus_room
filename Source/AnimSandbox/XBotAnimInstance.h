// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "XBotAnimInstance.generated.h"

class AAnimSandboxCharacter;

/**
 * 
 */
UCLASS()
class ANIMSANDBOX_API UXBotAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float PawnSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly) bool IsAirborne;
	UPROPERTY(EditAnywhere, BlueprintReadOnly) float WalkingRateScale;
	UPROPERTY(BlueprintReadOnly) bool bIsAlert;
	const float NormalizeToRangeOffset = 1.307692;

private:
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaTimeX) override;
	// void DebugPrint(float DeltaTimeX);
	float GetNormalizedSpeed(float Speed);

	APawn* OwningPawn;
};
