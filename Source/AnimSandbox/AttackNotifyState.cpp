// Fill out your copyright notice in the Description page of Project Settings.


#include "AttackNotifyState.h"
#include "AnimSandboxCharacter.h"
#include "Components/SkeletalMeshComponent.h"

void UAttackNotifyState::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
	AAnimSandboxCharacter* Character = Cast<AAnimSandboxCharacter>(MeshComp->GetOwner());
	if (Character == nullptr) { return; }
	Character->Weapon->SetCollisionProfileName(FName("Weapon"));
}

void UAttackNotifyState::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	AAnimSandboxCharacter* Character = Cast<AAnimSandboxCharacter>(MeshComp->GetOwner());
	if (Character == nullptr) { return; }
	Character->Weapon->SetCollisionProfileName(FName("NoCollision"));
}
