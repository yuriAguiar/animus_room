// Fill out your copyright notice in the Description page of Project Settings.


#include "GrabWeaponNotify.h"
#include "AnimSandboxCharacter.h"
#include "Components/SkeletalMeshComponent.h"

void UGrabWeaponNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	AAnimSandboxCharacter* Character = Cast<AAnimSandboxCharacter>(MeshComp->GetOwner());
	if (Character == nullptr) { return; }
	Character->Weapon->AttachToComponent(MeshComp, FAttachmentTransformRules::KeepRelativeTransform ,FName("RightHandSocket"));
}