// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "AnimSandboxCharacter.h"

#include "DrawDebugHelpers.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

#include "Components/SkeletalMeshComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "UObject/ConstructorHelpers.h"
#include "Animation/AnimMontage.h"
#include "Kismet/GameplayStatics.h"
#include "XBotAnimInstance.h"

//////////////////////////////////////////////////////////////////////////
// AAnimSandboxCharacter

AAnimSandboxCharacter::AAnimSandboxCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	// Create and attach the weapon mesh
	Weapon = CreateDefaultSubobject<UStaticMeshComponent>(FName("Custom Object"));
	Weapon->SetupAttachment(GetMesh(), FName("HipsSocket"));
	Weapon->SetCollisionProfileName(FName("NoCollision"));

	// Create and attach the shield mesh
	Shield = CreateDefaultSubobject<UStaticMeshComponent>(FName("Shield"));
	Shield->SetupAttachment(GetMesh(), FName("ShieldSocket"));
	Shield->SetCollisionProfileName(FName("NoCollision"));
	
	// Gets the class reference of the animation blueprint asset
	ConstructorHelpers::FClassFinder<UAnimInstance> DefaultAnimClass(TEXT("AnimBlueprint'/Game/Mixamo/Blueprints/XBotAnimBlueprint.XBotAnimBlueprint_C'"));
	ConstructorHelpers::FClassFinder<UAnimInstance> AlertAnimClass(TEXT("AnimBlueprint'/Game/Mixamo/Blueprints/XBotAnimBlueprint_Child.XBotAnimBlueprint_Child_C'"));

	if (DefaultAnimClass.Succeeded() && AlertAnimClass.Succeeded())
	{
		DefaultAnimInst = DefaultAnimClass.Class.Get();
		AlertAnimInst = AlertAnimClass.Class.Get();
	}

	ConstructorHelpers::FObjectFinder<UAnimMontage> WeaponActionsMontageObj(TEXT("AnimMontage'/Game/Mixamo/Blueprints/WeaponActions.WeaponActions'"));

	if (WeaponActionsMontageObj.Succeeded())
	{
		WeaponActionsMontage = WeaponActionsMontageObj.Object;
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AAnimSandboxCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AAnimSandboxCharacter::JumpInput);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AAnimSandboxCharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AAnimSandboxCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AAnimSandboxCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AAnimSandboxCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AAnimSandboxCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AAnimSandboxCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AAnimSandboxCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AAnimSandboxCharacter::OnResetVR);

	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &AAnimSandboxCharacter::Attack);
	PlayerInputComponent->BindAction("Attack", IE_Released, this, &AAnimSandboxCharacter::ReleasedAttackButton); // Debug
	
	// Testinput
	PlayerInputComponent->BindAction("TestInput", IE_Pressed, this, &AAnimSandboxCharacter::TestInput);
}

// Input for testing purposes
void AAnimSandboxCharacter::TestInput() { }

void AAnimSandboxCharacter::Attack()
{
	if (!XBotAnimInst->bIsAlert)
	{
		XBotAnimInst->bIsAlert = true;
		GetMesh()->SetAnimClass(AlertAnimInst);
		PlayAnimMontage(WeaponActionsMontage, 1.f, FName("StartWithdraw"));
		return;
	}

	bAttackPressed = true; // Debug

	// Generate a random number between 1 and 2
	int RandNum = rand() % 2 + 1;
	FString AttackNum = "StartAttack_" + FString::FromInt(RandNum);
	PlayAnimMontage(WeaponActionsMontage, 1.f, FName(*AttackNum));
}

void AAnimSandboxCharacter::JumpInput()
{
	Jump();
	bJumpPressed = true; // Debug
}

void AAnimSandboxCharacter::StopJumping()
{
	ACharacter::StopJumping();
	bJumpPressed = false; // Debug
}

void AAnimSandboxCharacter::BeginPlay()
{
	Super::BeginPlay();
	PlayerController = Cast<APlayerController>(Controller);
	XBotAnimInst = Cast<UXBotAnimInstance>(GetMesh()->GetAnimInstance());
}

void AAnimSandboxCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	float HoldTime = PlayerController->GetInputKeyTimeDown(EKeys::RightMouseButton);
	
	if (XBotAnimInst->bIsAlert && HoldTime > 1.f && bIsPossessed)
	{
		XBotAnimInst->bIsAlert = false;
		GetMesh()->SetAnimClass(DefaultAnimInst);
		PlayAnimMontage(WeaponActionsMontage, 1.f, FName("StartSheathing"));
	}

	if (bIsPossessed && bEnableDebugMessages)
	{
		DebugPrint();
	}
}

void AAnimSandboxCharacter::PossessedBy(AController* NewController)
{
	bIsPossessed = true;
}

void AAnimSandboxCharacter::UnPossessed()
{
	bIsPossessed = false;
}

void AAnimSandboxCharacter::DebugPrint() const
{
	DrawDebugString(GetWorld(),
		FVector(0.f, 0.f, 0.f),
		FString("Cassandra"),
		UGameplayStatics::GetPlayerCharacter(GetWorld(), 0),
		FColor::Cyan,
		0.f);

	if (bJumpPressed) { SetTextAndColorToDebugAndPrint(FString("Space Key - Jump: Pressed"), FColor::Yellow); }
	else { SetTextAndColorToDebugAndPrint(FString("Space Key - Jump: Released"), FColor::Cyan); }

	if (bAttackPressed) { SetTextAndColorToDebugAndPrint(FString("Left Mouse Key - Attack: Pressed"), FColor::Yellow); }
	else { SetTextAndColorToDebugAndPrint(FString("Left Mouse Key - Attack: Released"), FColor::Cyan); }

	if (XBotAnimInst->bIsAlert) { SetTextAndColorToDebugAndPrint(FString("State: Alert"), FColor::Yellow); }
	else { SetTextAndColorToDebugAndPrint(FString("State: Normal"), FColor::Cyan); }

	const FString CassandraDirectionAndRotationString = FString("Forward vector: ")
	.Append(LINE_TERMINATOR)
	.Append(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetActorForwardVector().ToString())
	.Append(LINE_TERMINATOR)
	.Append(LINE_TERMINATOR)
	.Append("Linear velocity:")
	.Append(LINE_TERMINATOR)
	.Append(GetVelocity().ToString());
	SetTextAndColorToDebugAndPrint(CassandraDirectionAndRotationString, FColor::Cyan);
}

void AAnimSandboxCharacter::SetTextAndColorToDebugAndPrint(const FString DebugString, const FColor DebugColor) const
{
	UKismetSystemLibrary::PrintString(
		GetWorld(),
        DebugString,
        true,
        false,
        DebugColor,
        0.f);
}

void AAnimSandboxCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AAnimSandboxCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	JumpInput();
}

void AAnimSandboxCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AAnimSandboxCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AAnimSandboxCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AAnimSandboxCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AAnimSandboxCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
