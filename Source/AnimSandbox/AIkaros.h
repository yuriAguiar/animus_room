// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/ArrowComponent.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/TimelineComponent.h"
#include "AIkaros.generated.h"

class UCameraComponent;
class USkeletalMeshComponent;
class UTimelineComponent;
class UCurveFloat;
class UCameraShake;

UCLASS()
class ANIMSANDBOX_API AAIkaros : public APawn
{
	GENERATED_BODY()

private:
	const float DEFAULT_FIELD_OF_VIEW = 90.f;
	
	UPROPERTY(VisibleAnywhere)
	USpringArmComponent* CameraBoom;
	
	UPROPERTY(VisibleAnywhere)
	USkeletalMeshComponent* EagleSkeleton;

	UPROPERTY(VisibleAnywhere)
	UArrowComponent* ArrowComponent;

	bool SpeedBoostTimelineSwitch = true;

	UFUNCTION()
	void SpeedBoostTimelineUpdateFunc(float DeltaSeconds) const;

	UFUNCTION()
	void ViewZoomTimelineUpdateFunc(float DeltaSeconds) const;

	// Debug functions
	void DebugPrint() const;
	void SetTextAndColorToDebugAndPrint(const FString DebugString, const FColor DebugColor) const;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Input functions
	void MoveForward(float Value);
	void MoveRight(float Value);
	void EnableFlightBoostCameraEffect();
	void DisableFlightBoostCameraEffect();
	void EnableViewZoomCameraEffect();
	void DisableViewZoomCameraEffect();
	void EnableStationaryMode();
	void DisableStationaryMode();
	void TagEnemy();
	void ReleaseTagEnemyButton();
	
public:
	// Sets default values for this pawn's properties
	AAIkaros();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* EagleMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* FollowCamera;

	// Ikaros's movement
	UPROPERTY(BlueprintReadWrite)
	bool bIsPossessed;

	UPROPERTY(BlueprintReadWrite)
	bool bIsStationary;

	UPROPERTY(BlueprintReadWrite)
	bool bBoostSpeed;

	const float PITCH_ROTATION_SPEED = 50.f;
	const float PITCH_STATIONARY_ANGLE = 45.f;
	
	UPROPERTY(BlueprintReadWrite)
	float DesiredPitch;

	const float ROLL_ROTATION_SPEED = 50.f;
	
	UPROPERTY(BlueprintReadWrite)
	float DesiredRoll;

	UPROPERTY(BlueprintReadWrite)
	float DesiredYaw;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float StabilizerSpeed = 2.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float YawRotationSpeed = .3f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float StationaryLinearDamping = 2.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MovingLinearDamping = .5f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Thrust = 5000.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float ThrustMultiplier = 5.f;

	// Ikaros's speed boost effect
	UPROPERTY(VisibleAnywhere)
	UTimelineComponent* SpeedBoostEffectTimeline;

	UPROPERTY(EditAnywhere)
	UCurveFloat* SpeedBoostTimelineCurve;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UCameraShake> CameraShakeClass;

	UPROPERTY(BlueprintReadWrite)
	UCameraShake* CameraShakeRef;

	FOnTimelineFloat SpeedBoostTimelineUpdateEvent;

	// Ikaros's enemy tag ability
	UPROPERTY(BlueprintReadWrite)
	bool bIsTagging;

	const float SPHERE_TRACE_RADIUS = 20.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float TagSphereTraceLengthMultiplier = 10000.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName EnemyTag = "Enemy";

	// Ikaros's view zoom ability
	UPROPERTY(BlueprintReadWrite)
	bool bIsZooming;

	UPROPERTY(VisibleAnywhere)
	UTimelineComponent* ViewZoomEffectTimeline;

	UPROPERTY(EditAnywhere)
	UCurveFloat* ViewZoomTimelineCurve;

	FOnTimelineFloat ViewZoomTimelineUpdateEvent;

	UPROPERTY(BlueprintReadWrite)
	bool bEnableDebugMessages;
};
