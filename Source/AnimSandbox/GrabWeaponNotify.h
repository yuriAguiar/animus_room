// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "GrabWeaponNotify.generated.h"

/**
 * 
 */
UCLASS()
class ANIMSANDBOX_API UGrabWeaponNotify : public UAnimNotify
{
	GENERATED_BODY()

private:
    virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
};
