// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AnimSandboxCharacter.generated.h"

class UAnimMontage;
class APlayerController;
class UXBotAnimInstance;

UCLASS(config=Game)
class AAnimSandboxCharacter : public ACharacter
{
	GENERATED_BODY()

public:
    AAnimSandboxCharacter();
       
    /** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
    float BaseTurnRate;
   
    /** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
    float BaseLookUpRate;
    
    /** Returns CameraBoom subobject **/
    FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
    /** Returns FollowCamera subobject **/
    FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

    UPROPERTY(BlueprintReadOnly)
    bool bIsPossessed;
	
    UPROPERTY(VisibleAnywhere)
    UStaticMeshComponent* Weapon;

    UPROPERTY(VisibleAnywhere)
    UStaticMeshComponent* Shield;

	// Animation
    UPROPERTY(BlueprintReadOnly)
    UXBotAnimInstance* XBotAnimInst;
    
    UClass* DefaultAnimInst;
    UClass* AlertAnimInst;

    // Only used for debug
    UPROPERTY(BlueprintReadOnly)
    bool bAttackPressed;

    UPROPERTY(BlueprintReadOnly)
    bool bJumpPressed;

	UPROPERTY(BlueprintReadWrite)
	bool bEnableDebugMessages;

    void ReleasedAttackButton();

protected:
	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);
	
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

private:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UPROPERTY()
	APlayerController* PlayerController;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* WeaponActionsMontage;
	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	void JumpInput();
	virtual void StopJumping() override;
	void Attack();
	virtual void PossessedBy(AController* NewController) override;
	virtual void UnPossessed() override;
	void TestInput();

	// Debug functions
    void DebugPrint() const;
    void SetTextAndColorToDebugAndPrint(const FString DebugString, const FColor DebugColor) const;
};

// Only used for debug
inline void AAnimSandboxCharacter::ReleasedAttackButton()
{
	bAttackPressed = false;
}

